function elementClicked ( name , description , level , categoryHeading , keyName ) {
    let type ;
    if ( description === "Catalog" ) {
      type = "catalog" ;
    } else if ( description === "Mix" ) {
      type = "mix" ;
    } else {
      type = "product" ;
    }
    document . cookie = "current_page=" + keyName ;
    document . cookie = "level=" + level ;
    $ . ajax ( {
        type : "POST" ,
        url : "/webApp/getHtml" ,
        data : JSON . stringify ( {
            "level" : level ,
            "category_heading" : categoryHeading ,
            "type" : type ,
            "title" : name ,
            "key_name" : keyName ,
            "user_id" : this . userId ,
            "color_scheme" : this . webApp . colorScheme ,
            "nav_bar_text" : $ ( "div.nav-bar" ) . html ()
        } ) ,
        contentType : "application/json" ,
        dataType : "html"
    } ) . done ( ( data ) => {
        $ ( "div.wrapper" ) . html ( data ) ;
        level += 1 ;
        if ( type === "product" ) {
            traverseForExistingProducts () ;
            initProductPageBehaviour () ;
        } else if ( type === "mix" ) {
            initWheelScrollEvent () ;
            traverseForExistingProducts () ;
            initProductPageBehaviour () ;
        } else {
            // if ( this . webApp . colorScheme === "dark" ) {
            //     $ ( "div.catalog-icon" ) . css ( "background-color" , "rgba( 255,255,255,0.15 )" ) ;
            // } else {
            //     $ ( "div.catalog-icon" ) . css ( "background-color" , "rgba( 0,0,0,0.15 )" ) ;
            // }
            $ ( "div.catalog-icon" ) . css ( "background-color" , "rgba( 255,255,255,0.15 )" ) ;
        }

    } ) ;
}

function initWheelScrollEvent () {
    let scrollContainer = $ ( "div.scroll-catalogs" ) ;

    scrollContainer . on ( 'wheel', ( evt ) => {
        evt . preventDefault () ;
        scrollContainer . scrollLeft ( scrollContainer . scrollLeft () + 0.5 * evt.originalEvent.deltaY ) ;
    } ) ;
}

function initProductPageBehaviour ( isCartPage = false ) {
    initModalEvents ( this . webApp , this . labels , this . lang ) ;
    $ ( "button.delete-btn" ) . off ( "click" ) ;
    $ ( "button.delete-btn" ) . on ( "click" , ( ev ) => {
      nullifyAmount () ;
    } ) ;
    // if ( this . webApp . colorScheme === "dark" ) {
    //     $ ( "div.dialog-box" ) . css ( "box-shadow" , "0 0 5px 5px rgba( 255,255,255,0.2 )" ) ;
    // } else {
    //     $ ( "div.dialog-box" ) . css ( "box-shadow" , "0 0 5px 5px rgba( 0,0,0,0.2 )" ) ;
    // }
    if ( isCartPage === true ) {
      $ ( "div.dialog-box-cart" ) . css ( "display" , "none" ) ;
      $ ( "div.dialog-box-cart" ) . css ( "box-shadow" , "0 0 5px 5px rgba( 0,0,0,0.2 )" ) ;
    } else {
      $ ( "div.dialog-box" ) . css ( "display" , "none" ) ;
      $ ( "div.dialog-box" ) . css ( "box-shadow" , "0 0 5px 5px rgba( 0,0,0,0.2 )" ) ;
    }
}

function traverseForExistingProducts () {
    let lang = this . lang ;
    let labels = this . labels ;
    // let inCartBtnText = labels [ lang ] . domButtons . inCart . text ;
    let elements = $ ( "div.in-cart-amount" ) ;
    $ . each ( elements , ( key , value ) => {
        let keyName = value . id . split ( '-' ) [ 0 ] ;
        $ . get ( "/webApp/getProductAmount" , { user_id : this . userId , key_name : keyName } , ( data , status ) => {
            if ( status === "success" && data > 0 ) {
                updateProductButtons ( keyName , data ) ;
            }
        } , "text" ) ;
    } ) ;

}

function updateProductButtons ( keyName , productAmount ) {
    $ ( '#' + keyName + "-to-cart-btn" ) . html ( '' ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ("background-image" , `url("data:image/svg+xml,%3C%3Fxml version='1.0' encoding='iso-8859-1'%3F%3E%3C!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 59.985 59.985' style='fill:white;enable-background:new 0 0 59.985 59.985;' xml:space='preserve'%3E%3Cg%3E%3Cpath d='M5.243,44.844L42.378,7.708l9.899,9.899L15.141,54.742L5.243,44.844z'/%3E%3Cpath d='M56.521,13.364l1.414-1.414c1.322-1.322,2.05-3.079,2.05-4.949s-0.728-3.627-2.05-4.949S54.855,0,52.985,0 s-3.627,0.729-4.95,2.051l-1.414,1.414L56.521,13.364z'/%3E%3Cpath d='M4.099,46.527L0.051,58.669c-0.12,0.359-0.026,0.756,0.242,1.023c0.19,0.19,0.446,0.293,0.707,0.293 c0.106,0,0.212-0.017,0.316-0.052l12.141-4.047L4.099,46.527z'/%3E%3Cpath d='M43.793,6.294l1.415-1.415l9.899,9.899l-1.415,1.415L43.793,6.294z'/%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3C/svg%3E%0A")`) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ( "background-size" , "20px 20px" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ( "background-repeat" , "no-repeat" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ( "background-position" , "center" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ( "background-color" , "#2F80ED" ) ;
    // $ ( '#' + keyName + "-to-cart-btn" ) . removeAttr ( "data-bs-toggle" ) ;
    // $ ( '#' + keyName + "-to-cart-btn" ) . removeAttr ( "data-bs-target" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . off ( "click" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . attr ( "data-book-id" , $ ( '#' + keyName + "-to-cart-btn" ) . attr ( "data-book-id" ) + "::edit" ) ;
    // $ ( '#' + keyName + "-to-cart-btn" ) . on ( "click" , ( e ) => {
    //     editClicked ( e ) ;
    // } ) ;
    let inCartBtn = "<button class='amount-button' id='" + keyName + "-amount-button'>" + productAmount + "</button>"
    $ ( '#' + keyName + "-amount-button" ) . css ( "display" , "grid" ) ;
    $ ( '#' + keyName + "-amount-button" ) . css ( "justify-content" , "center" ) ;
    $ ( '#' + keyName + "-in-cart-amount" ) . html ( inCartBtn ) ;
}

function revertProductButtons ( keyName ) {
    $ ( '#' + keyName + "-to-cart-btn" ) . html ( '+' ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ("background-image" , "none" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . css ( "background-color" , "#DBE8FF" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . attr ( "data-bs-toggle" , "modal" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . attr ( "data-bs-target" , "#add-to-cart-modal" ) ;
    $ ( '#' + keyName + "-to-cart-btn" ) . off ( "click" ) ;
    $ ( '#' + keyName + "-amount-button" ) . css ( "display" , "none" ) ;
    $ ( '#' + keyName + "-in-cart-amount" ) . html ( '' ) ;
}

function goToMain () {
    $ ( "div.wrapper" ) . load ( "/webApp/start" , { user_id : userId , color_scheme : webApp . colorScheme } ) ;
    resetMainButton () ;
}
