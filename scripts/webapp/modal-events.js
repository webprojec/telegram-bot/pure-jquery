function initModalEvents ( webApp , labels , lang ) {
    $ ( "#desc-modal" ) . on ( "show.bs.modal" , function ( event ) {
        let productInfo = $ ( event . relatedTarget ) . data ( 'book-id' ) ;
        let arrProductInfo = productInfo . split ( "::" ) ;
        let productDescription = arrProductInfo [ 0 ] ;
        let productName = arrProductInfo [ 1 ] ;
        let imagePath = arrProductInfo [ 2 ] ;
        let price = arrProductInfo [ 3 ] ;
        $ ( "#product-description" ) . html ( productDescription ) ;
        $ ( "#product-title" ) . html ( productName ) ;
        $ ( "div.modal-product-price" ) . find ( "span" ) . html ( beautifyThousands ( price ) ) ;
        $ ( "#product-image" ) . attr ( "src" , imagePath ) ;

        webApp . BackButton . hide () ;
    } ) ;
    $ ( "#desc-modal" ) . on ( "hidden.bs.modal" , () => {
        webApp . BackButton . show () ;
    } ) ;

    $ ( "#add-to-cart-modal" ) . on ( "show.bs.modal" , function ( event ) {
        let productInfo = $ ( event . relatedTarget ) . attr ( "data-book-id" ) ;
        let arrProductInfo = productInfo . split ( "::" ) ;
        let keyName = arrProductInfo [ 0 ] ;
        let price = arrProductInfo [ 1 ] ;
        let name = arrProductInfo [ 2 ] ;

        if ( arrProductInfo . length >= 4 && arrProductInfo [ 3 ] === "edit" ) {
            let changeBtnText = labels [ lang ] . modals . selectQuantity . changeBtn . text ;
            $ ( "#add-to-cart-btn" ) . html ( changeBtnText ) ;
            $ ( "#add-to-cart-btn" ) . off ( "click" ) ;
            $ ( "#add-to-cart-btn" ) . on ( "click" , function (e) {
              setInBasket ( keyName , true , arrProductInfo . length >= 5 ) ;
            } ) ;
        } else {
            let toCartBtnText = labels [ lang ] . domButtons . toCart . text ;
            $ ( "#add-to-cart-btn" ) . html ( toCartBtnText ) ;
            $ ( "#add-to-cart-btn" ) . off ( "click" ) ;
            $ ( "#add-to-cart-btn" ) . on ( "click" , function (e) {
              addToBasket ( keyName ) ;
            } ) ;
        }

        $ ( "span.product-name" ) . html ( name ) ;
        $ ( "#minus-amount" ) . off ( "click" ) ;
        $ ( "#minus-amount" ) . on ( "click" , function ( e ) {
          minusAmount ( keyName ) ;
        } ) ;
        $ ( "#plus-amount" ) . off ( "click" ) ;
        $ ( "#plus-amount" ) . on ( "click" , function ( e ) {
          plusAmount ( keyName ) ;
        } ) ;
        let theAmount = $ ( "#" + keyName + "-in-cart-amount" ) . find ( "button" ) . html () ;
        theAmount = theAmount ? theAmount : '0' ;
        $ ( "#the-amount" ) . html ( theAmount ) ;
        webApp . BackButton . hide () ;
    } ) ;

    $ ( "#add-to-cart-modal" ) . on ( "hidden.bs.modal" , () => {
        $ ( "#the-amount" ) . html ( '0' ) ;
        webApp . BackButton . show () ;
    } ) ;
}
