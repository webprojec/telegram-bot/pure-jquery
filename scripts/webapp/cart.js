function showBasket () {
    let userId = this . userId ;
    let colorScheme = this . webApp . colorScheme ;
    $ . ajax ( {
        type : "POST" ,
        url : "/webApp/showBasket" ,
        contentType : "application/json" ,
        data : JSON .stringify ( {
            "user_id" : userId ,
            "color_scheme" : colorScheme
        } ) ,
        dataType : "html"
    } ) . done ( ( cartPage ) => {
        $ ( "div.wrapper" ) . html ( cartPage ) ;

        let isCartPage = true ;
        initProductPageBehaviour ( isCartPage ) ;

        $ ( "svg.more-svg-btn" ) . off ( "click" ) ;
        $ ( "svg.more-svg-btn" ) . on ( "click" , (e) => {
          e. stopPropagation () ;
          editClickedInCart ( e ) ;
        } ) ;
        document . cookie = "isCart=true" ;
        let level = parseInt ( getCookie ( "level" ) ) + 1 ;
        document . cookie = "level=" + level ;
    } ) ;
}

function recalcTotalSums ( keyName ) {
    let newAmount = parseInt ( $ ( '#' + keyName + "-in-cart-amount" ) . find ( "button.amount-button" ) . html () )  ;
    let productPrice = parseInt ( $ ( '#' + keyName + "-product-price" ) . attr ( "data-book-id" ) ) ;
    let newProdTotalPrice = newAmount * productPrice ;
    let prevProdTotalPrice = parseInt ( $ ( '#' + keyName + "-product-total-price" ) . attr ( "data-book-id" ) ) ;
    let difference = newProdTotalPrice - prevProdTotalPrice ;
    let prevTotalPrice = parseInt ( $ ( "#total-sum" ) . attr ( "data-book-id" ) ) ;
    let newTotalPrice = prevTotalPrice + difference ;
    $ ( "#total-sum" ) . html ( beautifyThousands ( newTotalPrice ) ) ;
    $ ( '#' + keyName + "-product-total-price" ) . attr ( "data-book-id" , newProdTotalPrice ) ;
    $ ( '#' + keyName + "-product-total-price" ) . html ( beautifyThousands ( newProdTotalPrice ) ) ;
}

function removeProductEntry ( keyName ) {
    let toRemove = $ ( '#' + keyName + "-cart-element" ) ;
    let priceToSubstract = parseInt ( $ ( '#' + keyName + "-product-total-price" ) . attr ( "data-book-id" ) ) ;
    let totalSum = parseInt ( $ ( "#total-sum" ) . attr ( "data-book-id" ) ) ;
    totalSum -= priceToSubstract ;
    $ ( "#total-sum" ) . html ( beautifyThousands ( totalSum ) ) ;
    toRemove . remove () ;
}
