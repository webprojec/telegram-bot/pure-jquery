
const nameRegex = /[a-zA-Za-åa-ö-w-яҒғҲҳҚқ ]+$/gm ;
const phoneRegex = /[33|71|88|90|91|93|94|97|99]{1}\d{7}$/gm ;
const numberRegex = /\d+$/gm ;
const letterRegex = /[a-zA-Za-åa-ö-w-яҒғҲҳҚқ ]$/gm ;

const cardRegex = /^\d{16}$/gm ;
var canClose = false ;

(function($) {
  $.fn.inputFilter = function(callback) {
    return this.on("keyup drop", function(e) {
      if ( ! [ "Backspace" , "ArrayUp" , "ArrowDown" , "ArrowLeft" , "ArrowRight" , "Escape" , "Shift" ] . includes ( e . key ) ) {
          if (callback(this.value)) {
            // Accepted value
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            // Rejected value - restore the previous one
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            // Rejected value - nothing to restore
            this.value = "";
          }
      } else {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
      }

    });
  };
}(jQuery));

function showOrderPage () {
    let userId = this . userId ;
    if ( parseInt ( $ ( "#total-sum" ) . html () ) !== 0 ) {
        $ . ajax ( {
            type : "GET" ,
            url : "/webApp/showOrderPage" ,
            data : {
              user_id : this . userId
            } ,
            dataType : "html"
        } ) . done ( ( orderPage ) => {
            $ ( "div.wrapper" ) . html ( orderPage ) ;
            document . cookie = "level=-1" ;
            setInputValidation () ;
            $ ( "#phone" ) . val ( "" ) ;
            $ ( "#card" ) . val ( "" ) ;
            if ( $ ( "#paymenttype" ) . val () === "cash" ) {
                $ ( "#card" ) . parent () . css ( "display" , "none" ) ;
            }
        } ) ;
    }
}

function setInputValidation () {
    let labels = this . labels ;
    let lang = this . lang ;
    let phoneNumberError = labels [ lang ] . pages . order . errorMessages . phone ;
    $ ( "#name" ) . on ( "change" , () => {
        var a = $ ( "#name" ) . val () ;
        if ( ! nameRegex . test ( a ) ) {
            $ ( "#name-error" ) . css ( "visibility" , "visible" ) ;
        } else {
            $ ( "#name-error" ) . css ( "visibility" , "hidden" ) ;
        }
    } ) ;

    $ ( "#phone" ) . on ( "change" , () => {
        if ( ! phoneRegex . test ( $ ( "#phone" ) . val () ) ) {
            $ ( "#phone-error" ) . text ( phoneNumberError ) ;
            $ ( "#phone-error" ) . css ( "visibility" , "visible" ) ;
        } else {
            $ ( "#phone-error" ) . css ( "visibility" , "hidden" ) ;
        }
    } ) ;

    $( "#phone" ) . inputFilter ( function ( value ) {
        return numberRegex . test ( value ) ;
    } ) ;
    $( "#card" ) . inputFilter ( function ( value ) {
        return numberRegex . test ( value ) ;
    } ) ;

    $ ( "#name" ) . inputFilter ( function ( value ) {
        return letterRegex . test ( value ) ;
    } )

    $ ( "#card" ) . on ( "change" , () => {
        if ( ! cardRegex . test ( $ ( "#card" ) . val () ) ) {
            $ ( "#card-error" ) . css ( "visibility" , "visible" ) ;
        } else {
            $ ( "#card-error" ) . css ( "visibility" , "hidden" ) ;
        }
    } ) ;

    $ ( "#paymenttype" ) . on ( "change" , () => {
        if ( $ ( "#paymenttype" ) .val () == "card" ) {
            $ ( "#card" ) . prop ( "disabled" , false ) ;
            $ ( "#card" ) . parent () . css ( "display" , "block" ) ;
        } else {
            $ ( "#card" ) . prop ( "disabled" , true ) ;
            $ ( "#card" ) . parent () . css ( "display" , "none" ) ;
            $ ( "#card-error" ) . css ( "visibility" , "hidden" ) ;
        }
    } ) ;
}

function isNumberKey( evt ) {
    if ( ! numberRegex . test ( evt . key ) )
        return true;
    else
        return false;
}

function checkInputFields () {
    var hasError = false ;
    if ( $ ( "#name" ) . val () . length == 0 )
        hasError = true ;

    if ( $ ( "#phone" ) . val () == "" )
        hasError = true ;

    if ( ! $ ( "#card" ) . prop ( "disabled" ) && $ ( "#card" ) . val () == "" )
        hasError = true ;

    return hasError ? false : true ;
}

function makeOrder () {
    var name = $ ( "#name" ) . val () ;
    var phone = $ ( "#phone" ) . val () ;
    var paymentType = $ ( "#paymenttype" ) . find ( ":selected" ) . text () ;
    var isCard = false ;
    if ( paymentType == "UzCard" ) {
        var card = $ ( "#card" ) . val () ;
        isCard = true ;
    }

    var allErrors = $ ( ".error" ) . map ( function () {
        return this ;
    } ) . get () ;

    var isError = false ;
    allErrors . forEach ( error => {
        if ( $ ( error ) . css ( "visibility" ) === "visible" ) {
            isError = true ;
        }
    } ) ;
    if ( ! isError && checkInputFields () ) {
        canClose = true ;
        let userId = this . userId ;
        $ . ajax ( {
            url : "/webApp/saveOrder" ,
            type : "POST" ,
            data : JSON . stringify ( {
                user_id : userId ,
                name : name ,
                phone : phone ,
                card : isCard ? card : "" ,
                type : isCard ? "card" : "cash"
            } ) ,
            contentType : "application/json" ,
            dataType : "text" ,
        } ) . done ( (response) => {
            if ( response === "success" ) {
                this . webApp . showAlert ( "Buyurtmangiz qabul qilindi" , () => {
                    closeWebApp () ;
                    $ . get ( "/webApp/sendOrderDetails?user_id=" + this . userId ) ;
                } ) ;
            } else {
                this . webApp . showAlert ( "Xatolik yuz berdi !" ) ;
            }
        } ) ;
    }
}

function showProductInfo ( keyName ) {
    $ . ajax ( {
        type : "POST" ,
        url : "/webApp/getProductDescription" ,
        contentType : "application/json" ,
        data : JSON . stringify ( {
            "key_name" : keyName
        } ) ,
        dataType : "text"
    } ) . done ( ( description ) => {
        this . webApp . showAlert ( description ) ;
    } ) ;

}
