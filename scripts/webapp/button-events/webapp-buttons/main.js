function initMainButtonOnClick ( webApp ) {
    let userId = this . userId ;
    let mainBtn = webApp . MainButton ;
    let labels = this . labels ;
    let lang = this . lang ;
    setMainButton ( userId , mainBtn , labels , lang ) ;

    mainBtn . onClick ( () => {
        mainButtonClicked ( mainBtn ) ;
    } ) ;
}

function mainButtonClicked ( mainBtn ) {
    let labels = this . labels ;
    let text = mainBtn . text ;
    switch ( text ) {
        case labels . rus . mainBtn . cart :
        case labels . uzb . mainBtn . cart :
            let popupButtons = cartButtonPopups ( ) ;
            webApp . showPopup ( popupButtons , ( clickedBtn ) => {
                switch ( clickedBtn ) {
                    case "cart" :
                        showBasket () ;
                        if ( mainBtn . isVisible === true ) {
                            mainBtn . setText ( labels [ this . lang ] . mainBtn . order ) ;
                        }
                        document . cookie = "current_page=cart" ;
                        break ;
                    case "order" :
                        showOrderPage() ;
                        mainBtn . hide () ;
                        document . cookie = "current_page=order" ;
                        break ;
                }
            } ) ;
            break ;
        case labels . rus . mainBtn . order :
        case labels . uzb . mainBtn . order :
            showOrderPage() ;
            mainBtn . hide () ;
            break ;
        default :
            break ;
    }
}

function cartButtonPopups ( ) {
    let lang = this . lang ;
    let labels = this . labels ;

    let viewCart = {
        "id" : "cart" ,
        "text" : labels [ lang ] . mainBtn . cart
    } ;

    let goToOrder = {
        "id" : "order" ,
        "text" : labels [ lang ] . mainBtn . order
    } ;

    let popupButtons = [] ;
    popupButtons . push ( viewCart ) ;
    popupButtons . push ( goToOrder ) ;

    return {
        "title" : labels [ lang ] . popups . decideCartOrder . title ,
        "message" : labels [ lang ] . popups . decideCartOrder . message ,
        "buttons" : popupButtons
    }
}

function setMainButton ( userId , mainBtn , labels, lang , reset = false ) {
    $ . get ( "/webApp/isBasketEmpty?user_id=" + userId , ( data , status ) => {
        if ( status === "success" && data !== "true" ) {
            if ( mainBtn . isVisible === false || reset === true ) {
                mainBtn . setText ( labels [ lang ] . mainBtn . cart ) ;
                mainBtn . show () ;
            }
        }
    } ) ;
}

function resetMainButton () {
    setMainButton ( this . userId , this . webApp . MainButton , this . labels , this . lang , true ) ;
}
