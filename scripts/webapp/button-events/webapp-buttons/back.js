function initBackButtonOnClick ( webApp ) {
    let backButton = webApp . BackButton ;
    let mainBtn = webApp . MainButton ;
    let lang = this . lang ;
    let labels = this . labels ;
    backButton . show () ;
    backButton . onClick ( () => {
        $ . get ( "/webApp/isBasketEmpty?user_id=" + this . userId , ( data , status ) => {
            let orderBtnText = labels [ lang ] . mainBtn . order ;
            if ( status === "success" && data === "true" ) {
                mainBtn . hide () ;
            } else if ( mainBtn . text === labels [ lang ] . mainBtn . order ) {
                mainBtn . setText ( labels [ lang ] . mainBtn . cart ) ;
            }

            if ( getCookie ( "current_page" ) === "order" ) {
                mainBtn . show () ;
            }
        } ) ;

        let currentPage = getCookie ( "current_page" ) ;
        let level = getCookie ( "level" ) ;
        if ( currentPage === "root" ) {
            webApp . showConfirm ( labels [ lang ] . alerts . exit , ( confirm ) => {
                if ( confirm === true ) {
                    webApp . close () ;
                }
            } )
        } else if ( currentPage === "cart" || currentPage === "order" ) {
            backButtonClicked ( currentPage , -1 ) ;
            document . cookie = "current_page=root" ;
            if ( mainBtn . isVisible === false ) {
                mainBtn . show () ;
            }
        } else {
            backButtonClicked ( currentPage , level ) ;
        }
    } ) ;
}

function backButtonClicked ( currentPage , level ) {
    $ . ajax ( {
        type : "GET" ,
        url : "/webApp/getPrevPage" ,
        data : {
            key_name : currentPage ,
            level : level ,
            user_id : this . userId ,
            color_scheme : this . webApp . colorScheme ,
            nav_bar_text : $ ( "<div/>" ) . html ( $ ( "div.nav-bar" ) . html ( ) ).text ( )
        } ,
        dataType : "html"
    } ) . done ( ( html ) => {
        $ ( "div.wrapper" ) . html ( html ) ;

        $ . ajax ( {
            type : "GET" ,
            url : "/webApp/getPrevCatHeading" ,
            data : {
                key_name : currentPage ,
                level : level
            } ,
            dataType : "text"
        } ) . done ( ( currCatHeaing ) => {
            level -= 1 ;
            document . cookie = "current_page=" + currCatHeaing ;
            document . cookie = "level=" + level ;
        } ) ;
    } ) ;

}
