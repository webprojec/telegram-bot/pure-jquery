function plusAmount ( keyName ) {
    let amount = parseInt ( $ ( '#the-amount' ) . html () ) ;
    amount += 1 ;
    $ ( '#the-amount' ) . html ( amount ) ;
}

function minusAmount ( keyName ) {
    let amount = parseInt ( $ ( '#the-amount' ) . html () ) ;
    let lang = this . lang ;
    let labels = this . labels ;
    let toCartBtnText = labels [ lang ] . domButtons . toCart . text ;
    let inCartBtnText = labels [ lang ] . domButtons . inCart . text ;
    amount -= 1 ;
    if ( amount >= 1 ) {
        $ ( '#the-amount' ) . html ( amount ) ;
    } else if ( amount === 0 ) {
        $ ( '#the-amount' ) . html ( amount ) ;
    }
}

function addToBasket ( keyName , amountIsGiven = true ) {
    let amount = amountIsGiven === true ? parseInt ( $ ( "#the-amount" ) . html () ) : 1 ;
    let mainBtn = this . webApp . MainButton ;
    let lang = this . lang ;
    let labels = this . labels ;
    let zeroSelectedMessage = labels [ lang ] . warnings . zeroAmount ;
    let mainBtnCartBtnMessage = labels [ lang ] . mainBtn . cart ;
    if ( amount === 0 )
        this . webApp . showAlert ( zeroSelectedMessage ) ;
    else {
        $ . get ( "/webApp/addToBasket?user_id=" + this . userId + "&key_name=" + keyName + "&amount=" + amount , ( data , status ) => {
            if ( status === "success" ) {
                  $ ( "#add-to-cart-modal" ) . modal ( "hide" ) ;
                  $ . get ( "/webApp/getProductAmount" , { user_id : this . userId , key_name : keyName } , ( data , status ) => {
                      if ( status === "success" && data > 0 ) {
                          updateProductButtons ( keyName , data ) ;
                          if ( mainBtn . isVisible === false ) {
                                  mainBtn . setText ( mainBtnCartBtnMessage ) ;
                                  mainBtn . show () ;
                          }
                      }
                  } , "text" ) ;
            }
        } ) ;

    }
}

function setInBasket ( keyName , amountIsGiven = true , isCartPage = false ) {
    let amount = amountIsGiven === true ? parseInt ( $ ( "#the-amount" ) . html () ) : amountIsGiven ;
    let mainBtn = this . webApp . MainButton ;
    let lang = this . lang ;
    let labels = this . labels ;
    let userId = this . userId ;

    $ . ajax ( {
        type : "POST" ,
        url : "/webApp/setInBasket" ,
        data : JSON.stringify ( {
            user_id : userId ,
            key_name : keyName ,
            amount : amount
        } ) ,
        contentType : "application/json" ,
        dataType : "text"
    } ) . done ( ( isSetResponse ) => {
        let response = JSON . parse ( isSetResponse ) ;
        if ( response . status !== "success" ) {
            console . log ( response . errorMessage ) ;
            $ ( "#add-to-cart-modal" ) . modal ( "hide" ) ;
        } else {
            if ( response . newAmount === 0 ) {
                if ( isCartPage === true ) {
                    removeProductEntry ( keyName ) ;
                } else {
                    revertProductButtons ( keyName ) ;
                }
                $ . get ( "/webApp/isBasketEmpty?user_id=" + this . userId , ( data , status ) => {
                    if ( status === "success" && data === "true" ) {
                        if ( isCartPage === true ) {
                            $ ( "#total-sum" ) . html ( '0' ) ;
                            goToMain () ;
                        }
                        if ( mainBtn . isVisible === true ) {
                            mainBtn . hide () ;
                        }
                    }
                } ) ;

            } else {
                updateProductButtons ( keyName , response . newAmount ) ;
                if ( isCartPage === true ) {
                    recalcTotalSums ( keyName ) ;
                }
            }
            $ ( "#add-to-cart-modal" ) . modal ( "hide" ) ;
        }


    } ) ;
}

function editClicked ( e ) {
    let keyName = $ ( e . currentTarget ) . attr ( "data-book-id" ) . split ( "::" ) [ 0 ] ;
    $ ( "div.dialog-box" ) . css ( "display" , "grid" ) ;
    $ ( "div.dialog-box" ) . offset ( { top : $ ( e . currentTarget ) . offset() . top + 36 , left : $ ( e . currentTarget ) . offset() . left - 130 } ) ;
    $ ( "div.dialog-edit" ) . attr ( "data-bs-toggle" , "modal" ) ;
    $ ( "div.dialog-edit" ) . attr ( "data-bs-target" , "#add-to-cart-modal" ) ;
    $ ( "div.dialog-edit" ) . attr ( "data-book-id" , $ ( e . currentTarget ) . attr ( "data-book-id" ) + "::edit" ) ;
    // if ( $ ( "div.dialog-box" ) . css ( "display" ) === "none" || $ ( "div.dialog-box" ) . offset () . top !== $ ( e . currentTarget ) . offset() . top + 36 ) {
    //     $ ( "div.dialog-box" ) . css ( "display" , "grid" ) ;
    //     $ ( "div.dialog-box" ) . offset ( { top : $ ( e . currentTarget ) . offset() . top + 36 , left : $ ( e . currentTarget ) . offset() . left - 130 } ) ;
    //     $ ( "div.dialog-edit" ) . attr ( "data-bs-toggle" , "modal" ) ;
    //     $ ( "div.dialog-edit" ) . attr ( "data-bs-target" , "#add-to-cart-modal" ) ;
    //     $ ( "div.dialog-edit" ) . attr ( "data-book-id" , $ ( e . currentTarget ) . attr ( "data-book-id" ) + "::edit" ) ;
    //     $ ( "img" ) . removeAttr ( "data-bs-toggle" ) ;
    //     $ ( "img" ) . removeAttr ( "data-bs-target" ) ;
    //     $ ( "div.product-data" ) . removeAttr ( "data-bs-toggle" ) ;
    //     $ ( "div.product-data" ) . removeAttr ( "data-bs-target" ) ;
    //     $ ( "div.dialog-unselect" ) . off ( "click" ) ;
    //     $ ( "div.dialog-unselect" ) . on ( "click" , (ev) => {
    //         setInBasket ( keyName , 0 ) ;
    //         console.log("b");
    //     } ) ;
    //
    //     $ ( "div.dialog-unselect" ) . html ( keyName ) ;
    // } else {
    //     $ ( "div.dialog-box" ) . css ( "display" , "none" ) ;
    //     $ ( "img" ) . attr ( "data-bs-toggle" , "modal" ) ;
    //     $ ( "img" ) . attr ( "data-bs-target" , "#desc-modal" ) ;
    //     $ ( "div.product-data" ) . attr ( "data-bs-toggle" , "modal" ) ;
    //     $ ( "div.product-data" ) . attr ( "data-bs-target" , "#desc-modal" ) ;
    // }
    // console.log( e );
}

function editClickedInCart ( e ) {
    let keyName = $ ( e . currentTarget ) . attr ( "data-book-id" ) . split ( "::" ) [ 0 ] ;
    let clickEventTop = $ ( e . currentTarget ) . offset() . top + 36 ;
    let dialogBoxTop = $ ( $ ( "div.dialog-box-cart" ) . offset () . top ) ;
    let deltaTopPosition = Math . abs ( clickEventTop - dialogBoxTop ) ;
    if ( $ ( "div.dialog-box-cart" ) . css ( "display" ) === "none" || deltaTopPosition < 0 ) {
        $ ( "div.dialog-box-cart" ) . css ( "display" , "grid" ) ;
        $ ( "div.dialog-box-cart" ) . offset ( { top : $ ( e . currentTarget ) . offset() . top + 36 , left : $ ( e . currentTarget ) . offset() . left - 130 } ) ;
        $ ( "div.dialog-edit" ) . attr ( "data-bs-toggle" , "modal" ) ;
        $ ( "div.dialog-edit" ) . attr ( "data-bs-target" , "#add-to-cart-modal" ) ;
        $ ( "div.dialog-edit" ) . attr ( "data-book-id" , $ ( e . currentTarget ) . attr ( "data-book-id" ) + "::edit::cartID" ) ;
        $ ( "div.dialog-unselect" ) . off ( "click" ) ;
        $ ( "div.dialog-unselect" ) . on ( "click" , (ev) => {
            setInBasket ( keyName , 0 , true ) ;
            console.log("c");
        } ) ;
    } else {
        $ ( "div.dialog-box-cart" ) . css ( "display" , "none" ) ;
    }
}

function nullifyAmount ( ev ) {
    $ ( "#the-amount" ) . html ( '0' ) ;
}
