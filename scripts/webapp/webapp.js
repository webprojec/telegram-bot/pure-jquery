function initWebapp ( ) {
    document . cookie = "current_page=root" ;
    document . cookie = "level=0" ;
    let webApp = this . webApp ;
    webApp . expand () ;
    initMainButtonOnClick ( webApp ) ;
    initBackButtonOnClick ( webApp ) ;
    initUserFeedback () ;

    let version = parseFloat ( webApp . version ) ;
    if ( version < 6.2 ) {
        alert ( this . labels [ this . lang ] . warnings . version ) ;
        closeWebApp () ;
    }
    $ ( document ) . on ( "click" , ( e ) => {
      if ( $ ( "div.dialog-box" ) . css ( "display" ) === "grid" && $ ( e.target ) [0] . parentNode.className !== "add-to-cart" ) {
          $ ( "div.dialog-box" ) . css ( "display" , "none" ) ;
          $ ( "img" ) . attr ( "data-bs-toggle" , "modal" ) ;
          $ ( "img" ) . attr ( "data-bs-target" , "#desc-modal" ) ;
          $ ( "div.product-data" ) . attr ( "data-bs-toggle" , "modal" ) ;
          $ ( "div.product-data" ) . attr ( "data-bs-target" , "#desc-modal" ) ;
      }
      if ( $ ( "div.dialog-box-cart" ) . css ( "display" ) === "grid" && $ ( e.target ) [0] . parentNode.className !== "add-to-cart" ) {
          $ ( "div.dialog-box-cart" ) . css ( "display" , "none" ) ;
      }
    });
    webApp . MainButton . setParams ( { color : "#25B17B" } ) ;
}

function initUserFeedback () {
    $ ( document ) . on ( "mousedown" , "img, button, input, select" , function (event) {
        $ ( event.currentTarget ) . css ( "opacity" , "0.7" ) ;
        setTimeout ( () => {
            $ ( event.currentTarget ) . css ( "opacity" , "1" ) ;
        } , 200 ) ;
    } ) ;
}

function getCookie ( cookieName ) {
    var allCookies = document . cookie ;
    var arrAllCookies = allCookies . split ( ';' ) ;
    for (let index = 0; index < arrAllCookies.length; index++) {
        const element = arrAllCookies[index];
        var entry_key_value = element . split ( '=' ) ;
        var key = entry_key_value [ 0 ] ;
        key = key . replace ( / /g, '' ) ;
        var value = entry_key_value [ 1 ] ;
        value = value . replace ( / /g,'' ) ;
        if ( key === cookieName ) {
            return value ;
        }
    }
    return false ;
}

function setLangCookie ( userId ) {
    $ . get ( "/webApp/getLang" , { user_id : userId } , ( data , status ) => {
        if ( status === "success" ) {
            document . cookie = "lang=" + data ;
        }
    } , "text" ) ;
}

function beautifyThousands ( number ) {
    return number . toString () .replace ( /\B(?=(\d{3})+(?!\d))/g , " " )
}

function closeWebApp () {
    this . webApp . close () ;
}
