<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property string $user_id
 * @property string $customer_name
 * @property string $customer_phone
 * @property string $customer_paym_type
 * @property string $customer_card
 * @property string $basket
 * @property string $date
 * @property integer $dispatched
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'required'),
			array('dispatched', 'numerical', 'integerOnly'=>true),
			array('user_id, customer_card', 'length', 'max'=>20),
			array('customer_name', 'length', 'max'=>50),
			array('customer_phone', 'length', 'max'=>15),
			array('customer_paym_type', 'length', 'max'=>10),
			array('basket', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, customer_name, customer_phone, customer_paym_type, customer_card, basket, date, dispatched', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'customer_name' => 'Customer Name',
			'customer_phone' => 'Customer Phone',
			'customer_paym_type' => 'Customer Paym Type',
			'customer_card' => 'Customer Card',
			'basket' => 'Basket',
			'date' => 'Date',
			'dispatched' => 'Dispatched',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('customer_name',$this->customer_name,true);
		$criteria->compare('customer_phone',$this->customer_phone,true);
		$criteria->compare('customer_paym_type',$this->customer_paym_type,true);
		$criteria->compare('customer_card',$this->customer_card,true);
		$criteria->compare('basket',$this->basket,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('dispatched',$this->dispatched);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
