<?php

/**
 * This is the model class for table "catalog".
 *
 * The followings are the available columns in table 'catalog':
 * @property integer $id
 * @property string $name_uzb
 * @property string $name_rus
 * @property integer $sort
 * @property integer $level
 * @property string $type
 * @property integer $active
 * @property string $category_heading
 * @property string $key_name
 */
class Catalog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catalog';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sort, level, active', 'numerical', 'integerOnly'=>true),
			array('name_uzb, name_rus, type, key_name', 'length', 'max'=>50),
			array('category_heading', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_uzb, name_rus, sort, level, type, active, category_heading, key_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_uzb' => 'Name_uzb',
			'name_rus' => 'Name_rus',
			'sort' => 'Sort',
			'level' => 'Level',
			'type' => 'Type',
			'active' => 'Active',
			'category_heading' => 'Category Heading',
			'key_name' => 'Key Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_uzb',$this->name,true);
		$criteria->compare('name_rus',$this->name,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('level',$this->level);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('category_heading',$this->category_heading,true);
		$criteria->compare('key_name',$this->key_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Catalog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
