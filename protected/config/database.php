<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	
	'connectionString' => 'mysql:host=localhost;dbname=test01',
	'emulatePrepare' => true,
	'username' => 'test01',
	'password' => 'nE8sM4eD8v',
	'charset' => 'utf8',
);