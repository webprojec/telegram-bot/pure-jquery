<?php

    function set_menu_button ( $user_id , $text ) {
        $web_app_url = WEBAPP_URL . "?user_id=" . $user_id ;
        $url = TELEGRAM_API_URL . TOKEN . "/setChatMenuButton" ;

        $menu_button_webapp = [
            "type" => "web_app" ,
            "text" => $text ,
            "web_app" => [
                "url" => $web_app_url
            ]
        ];

        $post_fields = array (
            "chat_id" => $user_id,
            "menu_button" => json_encode ($menu_button_webapp , JSON_UNESCAPED_SLASHES)
        );

        $session = curl_init ();
        curl_setopt ($session , CURLOPT_HTTPHEADER , array (
            "Content-Type:multipart/form-data"
        ) ) ;
        curl_setopt ( $session , CURLOPT_URL , $url ) ;
        curl_setopt ( $session , CURLOPT_RETURNTRANSFER , 1 ) ;
        curl_setopt ( $session , CURLOPT_POSTFIELDS , $post_fields ) ;
        $response = curl_exec ( $session ) ;
    }

	  function rewind_basket ( $user_id ) {
		    $user_state = State :: model () -> find ( "user_id='$user_id'" ) ;
        $user_state -> basket = null ;
        $user_state -> save () ;
	  }

    function is_dialog_box_set () {
        $file_content = file_get_contents ( ROOT_PATH . "/message-tracker.json" ) ;
        $message_tracker = json_decode ( $file_content , true ) ;
        if ( isset ( $message_tracker [ $user_id ] [ "message_id" ] ) ) {
          return $message_tracker [ $user_id ] [ "message_id" ] ;
        } else {
          return false ;
        }
    }

    function delete_message ( $user_id , $message_id ) {
        $token = TOKEN ;
        $url = TELEGRAM_API_URL . TOKEN . "/deleteMessage" ;
        $post_fields = array (
            "chat_id" => $user_id ,
            "message_id" => $message_id
        );
        $session = curl_init ();
        curl_setopt ($session , CURLOPT_HTTPHEADER , array (
            "Content-Type:multipart/form-data"
        ) ) ;
        curl_setopt ( $session , CURLOPT_URL , $url ) ;
        curl_setopt ( $session , CURLOPT_RETURNTRANSFER , 1 ) ;
        curl_setopt ( $session , CURLOPT_POSTFIELDS , $post_fields ) ;
        $response = curl_exec ( $session ) ;
        curl_close ( $session ) ;
        return $response ;
    }

    function edit_message ( $user_id , $message_id , $text , $menu ) {
        $token = TOKEN ;
        $url = TELEGRAM_API_URL . TOKEN . "/editMessageText" ;
        $post_fields = array (
            "chat_id" => $user_id ,
            "message_id" => $message_id ,
            "text" => $text ,
            "parse_mode" => "HTML" ,
            "reply_markup" => $menu
        );
        $session = curl_init ();
        curl_setopt ($session , CURLOPT_HTTPHEADER , array (
            "Content-Type:application/json"
        ) ) ;
        curl_setopt ( $session , CURLOPT_URL , $url ) ;
        curl_setopt ( $session , CURLOPT_RETURNTRANSFER , 1 ) ;
        curl_setopt ( $session , CURLOPT_POSTFIELDS , json_encode ( $post_fields , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ) ;
        $response = curl_exec ( $session ) ;
        curl_close ( $session ) ;
        return $response ;
    }

    function get_lang ( $user_id ) {
        try {
            $lang = State :: model () -> find ( "user_id=$user_id" ) -> lang ;
        } catch ( \Exception $ex ) {
            send_message ( $user_id , "ERROR!\nMESSAGE :\t" . $ex -> getMessage () ) ;
        }
        return $lang ;
    }

    function set_lang ( $user_id , $lang ) {
        try {
            $user_state = State :: model () -> find ( "user_id=$user_id" ) ;
            $user_state -> lang = $lang ;
            $user_state -> save () ;
        } catch ( \Exception $ex ) {
            send_message ( $user_id , "ERROR!\nMESSAGE :\t" . $ex -> getMessage () ) ;
            return false ;
        }
        return true ;

    }

    function navigate_to ( $user_id , $to_where ) {
        $message_tracker = json_decode ( file_get_contents ( ROOT_PATH . "/message-tracker.json" ) , true ) ;
        $lang = get_lang ( $user_id ) ;
        if ( $to_where === "main" ) {
            $webapp_menu = INLINE_MENUS [ $to_where ] [ $lang ] [ "menu" ] ;
            $webapp_menu [ 0 ] [ 0 ] [ "web_app" ] [ "url" ] .= $user_id ;
            edit_message ( $user_id , $message_tracker [ $user_id ] [ "message_id" ] , INLINE_MENUS [ $to_where ] [ $lang ] [ "title" ] , json_encode ( ["inline_keyboard" => $webapp_menu ] , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ) ;
        } else {
            edit_message ( $user_id , $message_tracker [ $user_id ] [ "message_id" ] , INLINE_MENUS [ $to_where ] [ $lang ] [ "title" ] , json_encode ( ["inline_keyboard" => INLINE_MENUS [ $to_where ] [ $lang ] [ "menu" ] ] , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ) ;
        }
    }
?>
