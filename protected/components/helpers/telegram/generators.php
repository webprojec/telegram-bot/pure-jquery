<?php
    function generate_home_button ( $style ) {
        $home_svg_btn = <<< EOF
            <?xml version="1.0" encoding="iso-8859-1"?>
            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="22px" height="22px" viewBox="0 0 547.596 547.596" style="enable-background:new 0 0 547.596 547.596;$style"
             xml:space="preserve">
            <g>
            <path d="M540.76,254.788L294.506,38.216c-11.475-10.098-30.064-10.098-41.386,0L6.943,254.788
              c-11.475,10.098-8.415,18.284,6.885,18.284h75.964v221.773c0,12.087,9.945,22.108,22.108,22.108h92.947V371.067
              c0-12.087,9.945-22.108,22.109-22.108h93.865c12.239,0,22.108,9.792,22.108,22.108v145.886h92.947
              c12.24,0,22.108-9.945,22.108-22.108v-221.85h75.965C549.021,272.995,552.081,264.886,540.76,254.788z"/>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            </svg>
        EOF ;
        return $home_svg_btn ;
    }

    function generate_cancel_button ( $style ) {
        $cancel_svg_btn = <<< EOF
            <?xml version="1.0" encoding="iso-8859-1"?>
            <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            width="17" height="17" viewBox="0 0 47.095 47.095" style="enable-background:new 0 0 47.095 47.095;$style" xml:space="preserve">
            <g>
            <path d="M45.363,36.234l-13.158-13.16l12.21-12.21c2.31-2.307,2.31-6.049,0-8.358c-2.308-2.308-6.05-2.307-8.356,0l-12.212,12.21
            L11.038,1.906c-2.309-2.308-6.051-2.308-8.358,0c-2.307,2.309-2.307,6.049,0,8.358l12.81,12.81L1.732,36.831
            c-2.309,2.31-2.309,6.05,0,8.359c2.308,2.307,6.049,2.307,8.356,0l13.759-13.758l13.16,13.16c2.308,2.308,6.049,2.308,8.356,0
            C47.673,42.282,47.672,38.54,45.363,36.234z"/>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            </svg>
        EOF ;
        return $cancel_svg_btn ;
    }

    function generate_dialog_box ( $dialog_edit_btn_text , $dialog_unselect_btn_text ) {
        $dialog_box = <<< EOF
            <div class = "dialog-box">
              <div class = "dialog-edit">
                <button>$dialog_edit_btn_text</button>
              </div>
              <div class = "dialog-unselect">
                <button>$dialog_unselect_btn_text</button>
              </div>
            </div>
        EOF ;
        return $dialog_box ;
    }

    function generate_dialog_box_cart ( $dialog_edit_btn_text , $dialog_unselect_btn_text ) {
        $dialog_box = <<< EOF
            <div class = "dialog-box-cart">
              <div class = "dialog-edit">
                <button>$dialog_edit_btn_text</button>
              </div>
              <div class = "dialog-unselect">
                <button>$dialog_unselect_btn_text</button>
              </div>
            </div>
        EOF ;
        return $dialog_box ;
    }

    function generate_description_modal ( $user_id ) {
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        $modal_back_text = $labels [ $lang ] [ "modals" ] [ "description" ] ["backButton"] ;
        $label_price_single = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "elementLabels" ] [ "priceSingle" ] ;
        $label_currency = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "currencies" ] [ "uzs" ] ;
        $description_modal = <<< EOF
            <div class = "modal fade modal-style" id = "desc-modal" tabindex = "-1" aria-labelledby = "exampleModalLabel" aria-hidden = "true">
                <div class = "modal-dialog modal-dialog-centered">
                    <div class = "modal-content">
                        <div class = "modal-header">
                            <div class = "modal-title" id = "product-title">Modal title</div>
                        </div>
                        <div class = "modal-body">
                            <div class = "image-container"><img id = "product-image" /></div>
                            <div>
                              <div class = "modal-product-price">$label_price_single :&nbsp;<span></span> $label_currency</div>
                              <div id = "product-description"></div>
                            </div>
                        </div>
                        <div class = "modal-footer">
                            <button type = "button" class = "btn btn-primary" data-bs-dismiss = "modal">$modal_back_text</button>
                        </div>
                    </div>
                </div>
            </div>
        EOF;
        return $description_modal ;
    }

    function generate_add_to_cart_modal ( $user_id ) {
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        $btn_text_back = $labels [ $lang ] [ "modals" ] [ "description" ] ["backButton"] ;
        $btn_text_cancel = $labels [ $lang ] [ "modals" ] [ "description" ] ["cancelButton"] ;
        $btn_text_to_cart = $labels [ $lang ] ["domButtons"] [ "toCart" ] [ "text" ] ;
        $quantifier = $labels [ $lang ] [ "quantifier" ] ;
        $title = $labels [ $lang ] [ "modals" ] [ "selectQuantity" ] [ "title" ] ;
        //$cancel_svg_btn_style = $color_scheme === "dark" ? "fill: white" : "fill:#909499" ;
        $cancel_svg_btn_style = "fill:#909499" ;
        $cancel_svg_btn = generate_cancel_button ( $cancel_svg_btn_style ) ;
        $description_modal = <<< EOF
            <div class = "modal fade modal-style" id = "add-to-cart-modal" tabindex = "-1" aria-labelledby = "exampleModalLabel" aria-hidden = "true">
                <div class = "modal-dialog modal-dialog-centered">
                    <div class = "modal-content">
                        <div class = "modal-header">
                            <div class = "modal-title" id = "product-title">
                              <span>$title</span>
                              <span class="product-name">Product name</span>
                            </div>
                        </div>
                        <div class = "modal-body-to-cart">
                          <button class="add-to-cart" id="minus-amount">-</button>
                          <div class="product-amount-container"><span class = "product-amount" id ="the-amount">0</span><span class = "quantifier">$quantifier</span></div>
                          <button class="add-to-cart" id="plus-amount" >+</button>
                        </div>
                        <div class = "modal-footer">
                            <button type = "button" class = "btn btn-light delete-btn">
                              <svg x="0px" y="0px" width="22" height="22" viewBox="0 0 30 30" style=" fill:#FA3434;">
                              <path d="M 13 3 A 1.0001 1.0001 0 0 0 11.986328 4 L 6 4 A 1.0001 1.0001 0 1 0 6 6 L 24 6 A 1.0001 1.0001 0 1 0 24 4 L 18.013672 4 A 1.0001 1.0001 0 0 0 17 3 L 13 3 z M 6 8 L 6 24 C 6 25.105 6.895 26 8 26 L 22 26 C 23.105 26 24 25.105 24 24 L 24 8 L 6 8 z"></path>
                              </svg>
                            </button>
                            <button type = "button" class = "btn btn-light back-btn" data-bs-dismiss = "modal">$btn_text_cancel</button>
                            <button type = "button" class = "btn btn-success" id="add-to-cart-btn" >$btn_text_to_cart</button>
                        </div>
                    </div>
                </div>
            </div>
        EOF;
        return $description_modal ;
    }

    function generate_mix_page ( $catalogs , $products , $title , $user_id , $color_scheme , $products_level , $products_section , $product_cat_key_name , $nav_bar_text , $type = "mix" ) {
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        if ( $type === "root" ) {
          $page_title = '' ;
        } else {
          $page_title = "<div class='page-title' >$title</div>" ;
        };
        $dialog_edit_btn_text = $labels [ $lang ] [ "dialogBox" ] [ "edit" ] ;
        $dialog_unselect_btn_text = $labels [ $lang ] [ "dialogBox" ] [ "unselect" ] ;
        // $home_svg_btn_style = $color_scheme === "dark" ? "fill: white" : "fill:#909499" ;
        $home_svg_btn_style = "fill:#909499" ;
        $home_svg_btn = generate_home_button ( $home_svg_btn_style ) ;
        $html = "<div class = 'content-header'><div class='home-button' onclick='goToMain()'>$home_svg_btn</div><div class='page-data'><div class='nav-bar'>$nav_bar_text</div>$page_title</div></div>" ;
        $html .= generate_description_modal ( $user_id ) . generate_add_to_cart_modal ( $user_id ) ;
        $html .="<div class = 'web-app-container'><div class = 'scroll-catalogs'>" ;
        foreach ($catalogs as $key => $value) {
            $name = $value -> { "name_" . $lang } ;
            $id = $value -> id ;
            $image = $value -> key_name . ".png" ;
            $description = $value -> type ;
            $level = $value -> level ;
            $category_heading = $value -> category_heading ;
            $key_name = $value -> key_name ;
            $image_path = "/images/" . $image ;
            $image_path = @ file_exists ( ROOT_PATH . $image_path ) ? $image_path : "/images/folder.png" ;
            $buffer = <<< EOF
                <div class = 'catalog-container col-md-auto animate__animated animate__bounceIn' onclick = 'elementClicked( "$name" , "$description" , $level , "$category_heading" , "$key_name" ) '>
                    <!--<div class="catalog-icon-$color_scheme"><img src = '$image_path'></div>-->
                    <div class="catalog-icon-light"><img src = '$image_path'></div>
                    <div class="catalog-name">$name</div>
                    <!--<button class = 'normal-btn'> $name </button>-->
                </div>
            EOF;

            $html .= $buffer ;
        }
        $html .= "</div>" ;
        $html .= generate_dialog_box ( $dialog_edit_btn_text , $dialog_unselect_btn_text ) ;
        foreach ( $products as $key => $p_value ) {
            $p_section = $p_value -> section ;
            $p_key_name = $p_value -> key_name ;
            $p_image = $p_key_name . ".jpeg" ;
            $p_name = $p_value -> name ;
            $p_price = $p_value -> price ;
            $p_formatted_price = preg_replace ( '/\B(?=(\d{3})+(?!\d))/i' , " " , $p_price ) ;
            $p_description = $p_value -> { "description_" . $lang } ;
            $p_image_path = "/images/products/" . $p_section . "/" . $p_image ;
            $p_image_path = @ file_exists ( ROOT_PATH . $p_image_path ) ? $p_image_path : "/images/picture.png" ;
            $buffer = <<< EOF
                <div class = "product-container col-md-auto animate__animated animate__bounceIn" >
                    <img src = "$p_image_path" data-bs-toggle = "modal" data-bs-target = "#desc-modal" data-book-id = "$p_description::$p_name::$p_image_path::$p_price" />
                    <div class = "product-data" data-bs-toggle = "modal" data-bs-target = "#desc-modal" data-book-id = "$p_description::$p_name::$p_image_path::$p_price" >
                      <div class = "product-name">$p_name</div>
                      <div class = "product-price"><span>$p_formatted_price</span> UZS / 1</div>
                    </div>
                    <div class = "in-cart-amount" id = "$p_key_name-in-cart-amount" >
                    </div>
                    <div class = "add-to-cart" >
                      <button id="$p_key_name-to-cart-btn" data-bs-toggle = "modal" data-bs-target = "#add-to-cart-modal" data-book-id = "$p_key_name::$p_price::$p_name">+</button>
                    </div>
                </div>
            EOF ;
            $html .= $buffer ;
        }
        $html .= "</div>" ;
        return $html ;
    }
    function generate_catalog_page ( $catalogs , $title , $user_id , $color_scheme , $nav_bar_text , $type = "catalog" ) {
        $lang = get_lang ( $user_id ) ;
        if ( $type === "root" ) {
          $page_title = '' ;
        } else {
          $page_title = "<div class='page-title' >$title</div>" ;
        }
        // $home_svg_btn_style = $color_scheme === "dark" ? "fill: white" : "fill:#909499" ;
        $home_svg_btn_style = "fill:#909499" ;
        $home_svg_btn = generate_home_button ( $home_svg_btn_style ) ;

        $html = "<div class = 'content-header'><div class='home-button' onclick='goToMain()'>$home_svg_btn</div><div class='page-data'><div class='nav-bar'>$nav_bar_text</div>$page_title</div></div><div class = 'web-app-container'><div class = 'catalogs'>" ;
        foreach ($catalogs as $key => $value) {
            $name = $value -> { "name_" . $lang } ;
            $id = $value -> id ;
            $image = $value -> key_name . ".png" ;
            $description = $value -> type ;
            $level = $value -> level ;
            $category_heading = $value -> category_heading ;
            $key_name = $value -> key_name ;
            $image_path = "/images/" . $image ;
            $image_path = @ file_exists ( ROOT_PATH . $image_path ) ? $image_path : "/images/folder.png" ;
            $buffer = <<< EOF
                <div class = 'catalog-container col-md-auto animate__animated animate__bounceIn' onclick = 'elementClicked( "$name" , "$description" , $level , "$category_heading" , "$key_name" ) '>
                    <!--<div class="catalog-icon-$color_scheme"><img src = '$image_path'></div>-->
                    <div class="catalog-icon-light"><img src = '$image_path'></div>
                    <div class="catalog-name">$name</div>
                    <!--<button class = 'normal-btn'> $name </button>-->
                </div>
            EOF;

            $html .= $buffer ;
        }
        $html .= "</div></div>" ;
        return $html ;
    }

    function generate_product_page ( $products , $title , $user_id , $color_scheme , $nav_bar_text  ) {
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        $dialog_edit_btn_text = $labels [ $lang ] [ "dialogBox" ] [ "edit" ] ;
        $dialog_unselect_btn_text = $labels [ $lang ] [ "dialogBox" ] [ "unselect" ] ;
        // $home_svg_btn_style = $color_scheme === "dark" ? "fill: white" : "fill:#909499" ;
        $home_svg_btn_style = "fill:#909499" ;
        $home_svg_btn = generate_home_button ( $home_svg_btn_style ) ;

        $html = "<div class = 'content-header'><div class='home-button' onclick='goToMain()'>$home_svg_btn</div><div class='page-data'><div class='nav-bar'>$nav_bar_text</div><div class='page-title-products' >$title</div></div></div><!--<div id='console'></div> --><div class = 'web-app-container'>" ;
        $html .= generate_dialog_box ( $dialog_edit_btn_text , $dialog_unselect_btn_text ) ;
        foreach ( $products as $key => $value ) {
            $section = $value -> section ;
            $key_name = $value -> key_name ;
            $image = $key_name . ".jpeg" ;
            $name = $value -> name ;
            $price = $value -> price ;
            $formatted_price = preg_replace ( '/\B(?=(\d{3})+(?!\d))/i' , " " , $price ) ;
            $description = $value -> { "description_" . $lang } ;
            $image_path = "/images/products/" . $section . "/" . $image ;
            $image_path = @ file_exists ( ROOT_PATH . $image_path ) ? $image_path : "/images/picture.png" ;
            $buffer = <<< EOF
                <div class = "product-container col-md-auto animate__animated animate__bounceIn" >
                    <img src = "$image_path" data-bs-toggle = "modal" data-bs-target = "#desc-modal" data-book-id = "$description::$name::$image_path::$price" />
                    <div class = "product-data" data-bs-toggle = "modal" data-bs-target = "#desc-modal" data-book-id = "$description::$name::$image_path::$price" >
                      <div class = "product-name">$name</div>
                      <div class = "product-price"><span>$formatted_price</span> UZS / 1</div>
                    </div>
                    <div class = "in-cart-amount" id = "$key_name-in-cart-amount" >
                    </div>
                    <div class = "add-to-cart" >
                      <button id="$key_name-to-cart-btn" data-bs-toggle = "modal" data-bs-target = "#add-to-cart-modal" data-book-id = "$key_name::$price::$name">+</button>
                    </div>
                </div>
            EOF ;
            $html .= $buffer ;
        }
        $html .= "</div>" ;
        return generate_description_modal ( $user_id ) . generate_add_to_cart_modal ( $user_id ) . $html ;
    }

    function generate_cart_page ( $basket_detailed , $user_id , $color_scheme ) {
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        $page_title = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "title" ] ;

        $dialog_edit_btn_text = $labels [ $lang ] [ "dialogBox" ] [ "edit" ] ;
        $dialog_unselect_btn_text = $labels [ $lang ] [ "dialogBox" ] [ "unselect" ] ;

        $nav_bar_text = "Savat" ;
        // $home_svg_btn_style = $color_scheme === "dark" ? "fill: white" : "fill:#909499" ;
        $home_svg_btn_style = "fill:#909499" ;
        $home_svg_btn = generate_home_button ( $home_svg_btn_style ) ;
        $header = <<< EOF
            <div class = "content-header cart-header">
                <div class = "page-data">
                    <div class = "nav-bar" >$nav_bar_text</div>
                    <div class="page-title">$page_title</div>
                </div>
            </div>
        EOF ;
        $html = generate_add_to_cart_modal ( $user_id ) ;
        $html .= $header . "<div class = 'web-app-container'>" ;
        $html .= generate_dialog_box_cart ( $dialog_edit_btn_text , $dialog_unselect_btn_text ) ;
        $html .= "<div class = 'cart-container'>" ;

        $total_price = 0 ;
        $label_price_single = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "elementLabels" ] [ "priceSingle" ] ;
        $label_price_all = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "elementLabels" ] [ "priceAll" ] ;
        $label_total_price = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "totalSum" ] ;
        $label_currency = $labels [ $lang ] [ "pages" ] [ "cart" ] [ "currencies" ] [ "uzs" ] ;
        $label_quantifier = $labels [ $lang ] [ "quantifier" ] ;
        foreach ($basket_detailed as $key_name => $product_object) {
            $product = $product_object [ "object" ] ;
            $amount = $product_object [ "amount" ];
            $name = $product -> name ;
            $price = $product -> price ;
            $formatted_price = preg_replace ( '/\B(?=(\d{3})+(?!\d))/i' , " " , $price ) ;
            $description = $product -> {"description_" . $lang } ;
            $key_name = $product -> key_name ;
            $image = $key_name . ".jpeg" ;
            $section = $product -> section ;
            $product_total_price = $amount * $price ;
            $formatted_p_total_price = preg_replace ( '/\B(?=(\d{3})+(?!\d))/i' , " " , $product_total_price ) ;
            $total_price += $product_total_price ;
            $image_path = "/images/products/" . $section . "/" . $image ;
            $image_path = @ file_exists ( ROOT_PATH . $image_path ) ? $image_path : "/images/picture.png" ;
            $buffer = <<< EOF
                <div class = "cart-content-container" id = "$key_name-cart-element">
                    <div class = "cart-front-container">
                        <div class = "product-data" >
                            <div class = "product-name">$name</div>
                        </div>
                        <div class = "totals-calculated">
                            <div class = "more-svg">
                                <div class = "in-cart-amount" id = "$key_name-in-cart-amount" ><button class="amount-button">$amount</button></div>
                                <div class = "add-to-cart">
                                    <svg  data-book-id = "$key_name::$price::$name" class = "more-svg-btn" width="24px" height="24px" viewBox="0 0 24 24" style="fill:#909499" xmlns="http://www.w3.org/2000/svg">
                                        <g data-name="Layer 2">
                                            <g data-name="more-vertical">
                                              <rect width="24" height="24" transform="rotate(-90 12 12)" opacity="0"/>
                                              <circle cx="12" cy="12" r="2"/><circle cx="12" cy="5" r="2"/><circle cx="12" cy="19" r="2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class = "cart-product-prices">
                        <div class = "product-price"><span id = "$key_name-product-price" data-book-id="$price">$formatted_price</span>&nbsp;$label_currency&nbsp;/&nbsp;1&nbsp;$label_quantifier</div>
                        <div class = "amount-price"><span id = "$key_name-product-total-price" data-book-id = "$product_total_price">$formatted_p_total_price</span>&nbsp;$label_currency</div>
                    </div>
                </div>
            EOF ;
            $html .= $buffer ;
        }
        $formatted_total_price = preg_replace ( '/\B(?=(\d{3})+(?!\d))/i' , " " , $total_price ) ;
        $html .= "<div class = 'cart-content-container' ><div class='conclusion-container'><span class='total-sum-label'>$label_total_price&nbsp;:&nbsp;</span><span class = 'total-sum'><span id='total-sum' data-book-id='$total_price'>$formatted_total_price</span>&nbsp;$label_currency</span></div></div></div></div><br/><br/>" ;
        return $html ;
    }

    function generate_order_page ( $user_id ) {
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        $page_title = $labels [ $lang ] [ "pages" ] [ "order" ] [ "title" ] ;
        $nav_bar_text = $labels [ $lang ] [ "pages" ] [ "order" ] [ "navBarText" ] ;
        $title = $labels [ $lang ] [ "pages" ] [ "order" ] [ "title" ] ;

        $home_svg_btn_style = "fill:#909499" ;
        $home_svg_btn = generate_home_button ( $home_svg_btn_style ) ;

        $label_name_input = $labels [ $lang ] [ "pages" ] [ "order" ] [ "inputLabels" ] [ "name" ] ;
        $label_phone_input = $labels [ $lang ] [ "pages" ] [ "order" ] [ "inputLabels" ] [ "phone" ] ;
        $label_paymenttype_input = $labels [ $lang ] [ "pages" ] [ "order" ] [ "inputLabels" ] [ "paymenttype" ] ;
        $label_card_input = $labels [ $lang ] [ "pages" ] [ "order" ] [ "inputLabels" ] [ "card" ] ;

        $error_label_name = $labels [ $lang ] [ "pages" ] [ "order" ] [ "errorMessages" ] [ "name" ] ;
        $error_label_phone = $labels [ $lang ] [ "pages" ] [ "order" ] [ "errorMessages" ] [ "phone" ] ;
        $error_label_paymenttype = $labels [ $lang ] [ "pages" ] [ "order" ] [ "errorMessages" ] [ "paymenttype" ] ;
        $error_label_card = $labels [ $lang ] [ "pages" ] [ "order" ] [ "errorMessages" ] [ "card" ] ;

        $make_order_button_text = $labels [ $lang ] [ "domButtons" ] [ "makeOrder" ] [ "text" ] ;
        $paymenttype_opt1 = $labels [ $lang ] [ "domSelects" ] [ "paymenttype" ] [ "opt1" ] ;
        $paymenttype_opt2 = $labels [ $lang ] [ "domSelects" ] [ "paymenttype" ] [ "opt2" ] ;
        $html = <<< EOF
            <div class = 'content-header'>
                <div class='home-button' onclick='goToMain()'>$home_svg_btn</div>
                <div class='page-data'>
                    <div class='nav-bar'>$nav_bar_text</div>
                    <div class='page-title' >$title</div>
                </div>
            </div>
            <div class = "web-app-container">
                <div class = "cart-content-container order-props-container" >
                    <div class = "field" >
                        <label for="name">$label_name_input :</label>
                        <input type="text" id="name">
                        <label class = "error" id = "name-error">$error_label_name</label>
                    </div>
                    <div class = "field" >
                        <label for="phone">$label_phone_input :</label>
                        <div class = "input-box">
                          <span class = "prefix">+998</span>
                          <input class = "input-itself" type="text" id="phone">
                        </div>
                        <label class = "error" id = "phone-error">$error_label_phone</label>
                    </div>
                    <div class = "field" >
                        <label for="paymenttype">$label_paymenttype_input :</label>
                        <select id="paymenttype">
                            <option value="cash">$paymenttype_opt1</option>
                            <option value="card">$paymenttype_opt2</option>
                        </select>
                        <label class = "error" id = "payment-error">$error_label_paymenttype</label>
                    </div>
                    <div class = "field card-number" >
                        <label for="card">$label_card_input :</label>
                        <input class = "input-itself" type="text" id="card" disabled >
                        <label class = "error" id = "card-error">$error_label_card</label>
                    </div>
                    <div class = "submit-container" >
                        <button class = "btn btn-success" onclick = "makeOrder ()" >$make_order_button_text</button>
                    </div>
                </div>
            </div>
        EOF ;
        return $html ;
    }

    function generate_html ( $vars ) {
        $user_id = $vars [ "user_id" ] ;
        $lang = get_lang ( $user_id ) ;
        $labels = json_decode ( file_get_contents ( ROOT_PATH . "/labels.json" ) , true ) ;
        switch ( $vars [ "type" ] ) {
            case "root" :
                $catalogs = Catalog :: model () -> findAll ( "level=0") ;
                return generate_catalog_page ( $catalogs , $labels [ $lang ] [ "mainMenuTitle" ] , $user_id , $vars [ "color_scheme" ] , $vars [ "nav_bar_text" ] , 'root'  ) ;
                break ;
            case "catalog" :
                # catalog
                $vars [ "level" ] += 1 ;
                $catalogs = Catalog :: model () -> findAll ( "level=" . $vars [ "level" ] . " and category_heading='" . $vars [ "category_heading" ] . "'" ) ;
                return generate_catalog_page ( $catalogs , $vars [ "title" ] , $user_id , $vars [ "color_scheme" ] , $vars [ "nav_bar_text" ] ) ;
                break;
            case "product" :
                # product
                $products = Product :: model () -> findAll ( "section='" . $vars [ "key_name" ]. "'" ) ;
                return generate_product_page ( $products , $vars [ "title" ] , $user_id , $vars [ "color_scheme" ] , $vars [ "nav_bar_text" ] ) ;
                break;
            case "cart" :
                # cart
                return generate_cart_page ( $vars [ "basket_detailed" ] , $user_id , $vars [ "color_scheme" ] ) ;
                break;
            case "order" :
                # order
                return generate_order_page ( $user_id ) ;
                break;
            case "mix" :
                # mix ( catalog & product )
                $vars [ "level" ] += 1 ;
                $catalogs = Catalog :: model () -> findAll ( "level=" . $vars [ "level" ] . " and category_heading='" . $vars [ "category_heading" ] . "'" ) ;
                $products = Product :: model () -> findAll ( "section='" . $vars [ "key_name" ]. "'" ) ;
                if ( count ( $products ) === 0 ) {
                    return generate_catalog_page ( $catalogs , $vars [ "title" ] , $user_id , $vars [ "color_scheme" ] , $vars [ "nav_bar_text" ] ) ;
                } else {
                    return generate_mix_page ( $catalogs , $products , $vars [ "title" ] , $user_id , $vars [ "color_scheme" ] , $vars [ "level" ] , $vars [ "category_heading" ], $vars [ "key_name" ] , $vars [ "nav_bar_text" ] ) ;
                }
                break ;
        }
    }
?>
