<?php
    function send_message ( $user_id , $text ) {
        $url = TELEGRAM_API_URL . TOKEN . "/sendMessage" ;
        $post_fields = array (
            "chat_id" => $user_id,
            "text" => $text,
            "parse_mode" => "HTML"
        );
        $session = curl_init ();
        curl_setopt ($session , CURLOPT_HTTPHEADER , array (
            "Content-Type:multipart/form-data"
        ) ) ;
        curl_setopt ( $session , CURLOPT_URL , $url ) ;
        curl_setopt ( $session , CURLOPT_RETURNTRANSFER , 1 ) ;
        curl_setopt ( $session , CURLOPT_POSTFIELDS , $post_fields ) ;
        $response = curl_exec ( $session ) ;
        curl_close ( $session ) ;
        return $response ;
    }

    function send_lang_selector ( $user_id ) {
        $url = TELEGRAM_API_URL . TOKEN . "/sendMessage" ;
        $menu = json_encode ( [
            "inline_keyboard" => [
                [
                    [
                        "text" => "🇺🇿 O'zbekcha" ,
                        "callback_data" => "mix::main::lang::uzb"
                    ],
                    [
                        "text" => "🇷🇺 Русский" ,
                        "callback_data" => "mix::main::lang::rus"
                    ]
                ]
            ]
        ] , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) ;
        $post_fields = array (
            "chat_id" => $user_id ,
            "text" => "Tilni tanlang / Выберите язык" ,
            "reply_markup" => $menu
        );

        $session = curl_init ();
        curl_setopt ($session , CURLOPT_HTTPHEADER , array (
            "Content-Type:application/json"
        ) ) ;
        $opt_array = array (
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS =>  json_encode ( $post_fields , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ,
                CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array ($session , $opt_array ) ;
        $response = curl_exec ( $session ) ;
        curl_close ( $session ) ;
        return $response ;
    }

    function send_web_app_message ( $user_id ) {
        $url = TELEGRAM_API_URL . TOKEN . "/sendMessage" ;
        $lang = get_lang ( $user_id ) ;
        $webapp_menu = INLINE_MENUS [ "main" ] [ $lang ] [ "menu" ] ;
        $webapp_menu [ 0 ] [ 0 ] [ "web_app" ] [ "url" ] .= $user_id ;
        // send_message ( $user_id , json_encode ( INLINE_MENUS [ "main" ] [ $lang ] [ "menu" ] , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ) ;
        $menu = json_encode ( [
            "inline_keyboard" => $webapp_menu
        ] , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) ;
        $post_fields = array (
            "chat_id" => $user_id ,
            "text" => INLINE_MENUS [ "main" ] [ $lang ] [ "title" ] ,
            "reply_markup" => $menu
        );

        $session = curl_init ();
        curl_setopt ($session , CURLOPT_HTTPHEADER , array (
            "Content-Type:multipart/form-data"
        ) ) ;
        $opt_array = array (
                CURLOPT_URL => $url,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS =>  $post_fields ,
                CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array ($session , $opt_array ) ;
        $response = curl_exec ( $session ) ;
        curl_close ( $session ) ;
        return $response ;
    }

    function send_order_details ( $user_id ) {
    		$order = Orders :: model () -> findBySql ( "SELECT * FROM orders WHERE user_id=$user_id ORDER BY id DESC LIMIT 1" ) ;
    		$customer_name = $order -> customer_name ;
    		$customer_phone = $order -> customer_phone ;
    		$customer_paym_type = $order -> customer_paym_type ;
    		$customer_card = $order -> customer_card ;
    		$customer_basket = json_decode ( $order -> basket , true ) ;

        $order_content = "" ;
        $total_price = 0.0 ;
        $counter = 1 ;
    		foreach ( $customer_basket as $key => $value ) {
    			$product = Product :: model () -> find ("key_name='$key'") ;
    			$product_name = $product -> name ;
    			$product_price = $product -> price ;
    			$order_content .= "\t\t $counter) " . $product_name . " - " . $value . " ta" . " (".$value." x ".$product_price." = " .($value * $product_price)." so'm)\n" ;
    			$total_price += $value * $product_price ;
                $counter += 1 ;
    			// send_message ( $user_id , $order_item ) ;
    		}

        $order_details = "<b>$customer_name, buyurtmangiz haqida ma'lumot !</b>\n" ;
        $order_details .= "Telefon raqamingiz : <b><u>$customer_phone</u></b>\n" ;
        if ( $customer_paym_type !== "card" ) {
          $order_details .= "To'lash turi : <b><u>Naxt</u></b>\n" ;
    		} else {
    			$order_details .= "To'lash turi: <b><u>UzCard</u></b>\n" ;
    		}

        $order_details .= "<b>Buyurtma qilingan yeguliklar :</b>\n" ;
        $order_details .= $order_content . "\n" ;
        $order_details .= "<em>\t\t Umumiysi</em> : <b><u>" . $total_price . "</u></b> so'm" ;
		    send_message ( $user_id , $order_details ) ;
    }
?>
