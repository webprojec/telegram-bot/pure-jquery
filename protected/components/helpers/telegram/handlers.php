<?php
    function handle_message_id ( $user_id , $message_id ) {
        $file_content = file_get_contents ( ROOT_PATH . "/message-tracker.json" ) ;
        $message_tracker = json_decode ( $file_content , true ) ;
        $message_tracker [ $user_id ] [ "message_id" ] = $message_id ;
        $new_file_content = json_encode ( $message_tracker , JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ;
        file_put_contents ( ROOT_PATH . "/message-tracker.json" , $new_file_content ) ;
    }

    function handle_user_id ( $user_id ) {
        $user_exists = State :: model () -> exists ( "user_id=$user_id" ) ;
        if ( $user_exists === false ) {
            try {
                $new_record = new State();
                $new_record -> user_id = $user_id;
                $new_record -> save();
            } catch (Exception $ex) {
                send_message ( $user_id , "ERROR!\nMESSAGE :\t" . $ex -> getMessage () ) ;
            }
        }
    }

    function handle_command ( $user_id , $command ) {
        set_menu_button ( $user_id , "webapp" ) ;
    }

    function handle_text ( $user_id , $text ) {
        // send_message ( $user_id , $text ) ;
    }

    function handle_message_input ( $arrayed_input ) {
        $message = $arrayed_input [ "message" ] ;
        $message_id = $message [ "message_id" ] ;
        $user_id = $message [ "from" ] [ "id" ] ;
        $text = $message [ "text" ] ;

        delete_message ( $user_id , $message_id ) ;
        handle_user_id ( $user_id ) ;

        //
        $lang = get_lang ( $user_id ) ;
        if ( $lang === NULL ) {
            $response = json_decode ( send_lang_selector ( $user_id ) , true ) ;
            if ( isset ( $response [ "result" ] [ "message_id" ] ) ) {
                handle_message_id ( $user_id , $response [ "result" ] [ "message_id" ] ) ;
            }
        } else {
            $is_command = substr ( $text , 0 , 1 ) === '/';
            $is_command ? handle_command ( $user_id , $text ) : NULL ;
            $dialog_box = is_dialog_box_set ( ) ;
            if ( $dialog_box === false ) {
                $response = json_decode ( send_web_app_message ( $user_id ) , true ) ;
                // send_message ( $user_id , send_web_app_message ( $user_id ) ) ;
                if ( isset ( $response [ "result" ] [ "message_id" ] ) ) {
                    handle_message_id ( $user_id , $response [ "result" ] [ "message_id" ] ) ;
                }
            } else {
                $webapp_menu = INLINE_MENUS [ "main" ] [ $lang ] [ "menu" ] ;
                $webapp_menu [ 0 ] [ 0 ] [ "web_app" ] [ "url" ] .= $user_id ;
                $menu = json_encode ( [
                    "inline_keyboard" => $webapp_menu
                ] , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES ) ;
                file_put_contents ( ROOT_PATH . "/console" , "\n" . edit_message ( $user_id , $dialog_box , "Web App is already open!" , $menu ) , FILE_APPEND ) ;
            }
        }
    }

    function handle_callback_query ( $arrayed_input ) {
        $data = $arrayed_input [ "callback_query" ] [ "data" ] ;
        $user_id = $arrayed_input [ "callback_query" ] [ "message" ] [ "chat" ] [ "id" ] ;
        $arrayed_data = explode ( "::" , $data ) ;
        $control = $arrayed_data [ 0 ] ;

        switch ( $control ) {
            case "nav": // navigation
                $to_where = $arrayed_data [ 1 ] ;
                // send_message ( $user_id , "to_where : " . $to_where ) ;
                navigate_to ( $user_id , $to_where ) ;
                break;
            case "dat": // data
                $name = $arrayed_data [ 1 ] ;
                $value = $arrayed_data [ 2 ] ;
                handle_data ( $user_id , $name , $value ) ;
                // send_message ( $user_id , "name : " . $name . " ; value : " . $value ) ;
                break;
            case "mix" :
                $to_where = $arrayed_data [ 1 ] ;
                $name = $arrayed_data [ 2 ] ;
                $value = $arrayed_data [ 3 ] ;
                handle_data ( $user_id , $name , $value ) ;
                navigate_to ( $user_id , $to_where ) ;
            default:
                // send_message ( $user_id , "default" ) ;
                break;
        }
        // send_message ( $user_id , $data ) ;
    }

    function handle_data ( $user_id , $name , $value ) {
        switch ( $name ) {
            case "lang" :
                set_lang ( $user_id , $value ) ;
                break;

            default:
                // code...
                break;
        }
    }

    function handle_input ( $input ) {
        $arrayed_input = json_decode ( $input , true ) ;
        if ( isset ( $arrayed_input [ "message" ] ) ) {
            handle_message_input ( $arrayed_input ) ;
        } else if ( $arrayed_input [ "callback_query" ] ) {
            handle_callback_query ( $arrayed_input ) ;
        }
    }
?>
