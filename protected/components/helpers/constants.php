<?php
	define ( "TOKEN" , "5410805042:AAHzj0MEJZJpR_k4KopgPGETCGvk4SghRU4" ) ; // SampleBot
    // define ( "TOKEN" , "2200987546:AAEDRKa4ArQhKH--ARcmC-mBMFpf8J1T81o" ) ; // Test ( test mode )
	define ( "TELEGRAM_API_URL" , "https://api.telegram.org/bot" ) ;
	define ( "WEBAPP_URL" , "https://test01.salesdoc.io/telegram/webApp" ) ;
	define ( "SETTINGS_URL" , "https://test01.salesdoc.io/telegram/settings" ) ;
	define ( "ROOT_PATH" , $_SERVER ['DOCUMENT_ROOT'] ) ;


	define ( "INLINE_MENUS" , [
			"settings" => [
					"rus" => [
							"menu" => [
									[
											[
													"text" => "🇺🇿 O'zbekcha" ,
													"callback_data" => "mix::main::lang::uzb"
											],
											[
													"text" => "🇷🇺 Русский" ,
													"callback_data" => "mix::main::lang::rus"
											]
									] ,
									[
											[
													"text" => "◀️ Назад" ,
													"callback_data" => "nav::main"
											]
									]
							] ,
							"title" => "Настройки"

					] ,
					"uzb" => [
							"menu" => [
									[
											[
													"text" => "🇺🇿 O'zbekcha" ,
													"callback_data" => "mix::main::lang::uzb"
											],
											[
													"text" => "🇷🇺 Русский" ,
													"callback_data" => "mix::main::lang::rus"
											]
									] ,
									[
											[
													"text" => "◀️ Orqaga" ,
													"callback_data" => "nav::main"
											]
									]
							] ,
							"title" => "Sozlamalar"
					]
			] ,
			"main" => [
					"rus" => [
							"menu" => [
									[
	                    [
	                        "text" => "Открыть веб-приложение" ,
	                        "web_app" => [
	                            "url" => WEBAPP_URL . "?user_id="
	                        ]
	                    ]
	                ] ,
									[
											[
													"text" => "⚙️ 🔧 Настройки" ,
													"callback_data" => "nav::settings"
											]
									]
							] ,
							"title" => "Веб-приложение"
					] ,
					"uzb" => [
							"menu" => [
									[
	                    [
	                        "text" => "Veb-ilovani ochish" ,
	                        "web_app" => [
	                            "url" => WEBAPP_URL . "?user_id="
	                        ]
	                    ]
	                ],
									[
											[
													"text" => "⚙️ 🔧 Sozlamalar" ,
													"callback_data" => "nav::settings"
											]
									]
							] ,
							"title" => "Veb-ilova"
					]
			]

	] ) ;
?>
