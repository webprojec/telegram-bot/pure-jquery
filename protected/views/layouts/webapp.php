<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>Document</title>

        <!-- Telegram Web App script , note -> 'version' variable in the script was manually changed to 6.2, before it was 6.0 -->
    		<script src="<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/telegram-web-app.js"></script>
    		<!-- jQuery -->
    		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

    		<!-- Bootstrap script & styles -->
    		<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
    		<script src="<?php echo Yii::app()->request->baseUrl; ?>/scripts/bootstrap/bootstrap.bundle.min.js"></script>

    		<!-- Animation styles -->
    		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate/animate.min.css">

        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/webapp.js" ></script>
        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/grid-element.js" ></script>
        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/cart.js" ></script>
        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/order.js" ></script>

        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/button-events/html-buttons/button-events.js" ></script>
        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/modal-events.js" ></script>
        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/button-events/webapp-buttons/back.js" ></script>
        <script src = "<?php echo Yii::app()->request->baseUrl; ?>/scripts/webapp/button-events/webapp-buttons/main.js" ></script>

        <!-- <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp/webapp.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp/catalog.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp/product.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp/cart.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp/order.css"> -->
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp_style1/webapp.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp_style1/product.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp_style1/modals.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp_style1/catalog.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp_style1/cart.css">
        <link rel="stylesheet" type="text/css" href = "<?php echo Yii::app()->request->baseUrl; ?>/css/webapp_style1/order.css">

    </head>
    <body>
        <?php echo $content; ?>
    </body>
</html>
