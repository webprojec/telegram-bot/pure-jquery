<?php

class WebAppController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionStart () {
		parse_str ( file_get_contents ( "php://input" ) , $arrayed_input ) ;
		$user_id = $arrayed_input [ "user_id" ] ;
		$color_scheme = $arrayed_input [ "color_scheme" ] ;
		$html = generate_html ( [ "type" => "root" , "user_id" => $user_id , "color_scheme" => $color_scheme , "nav_bar_text" => "Main" ] ) ;
		echo $html ;
	}

	public function actionGetLang () {
		$user_id = $_GET [ "user_id" ] ;
		echo State :: model () -> find ( "user_id=$user_id" ) -> lang ;
	}

	public function actionGetHtml () {
		$_POST = json_decode ( file_get_contents ( "php://input" ) , true ) ;
		$level = $_POST [ "level" ] ;
		$category_heading = $_POST [ "category_heading" ] ;
		$type = $_POST [ "type" ] ;
		$title = $_POST [ "title" ] ;
		$key_name = $_POST [ "key_name" ] ;
		$user_id = $_POST [ "user_id" ] ;
		$color_scheme = $_POST [ "color_scheme" ] ;
		$nav_bar_text = $_POST [ "nav_bar_text" ] ;
		$html = generate_html ( [
			"type" => $type ,
			"level" => $level ,
			"title" => $title ,
			"category_heading" => $category_heading ,
			"key_name" => $key_name ,
			"user_id" => $user_id ,
			"color_scheme" => $color_scheme ,
			"nav_bar_text" => $nav_bar_text . " > " . $title
		] ) ;
		echo $html ;
	}

	public function actionGetProductDescription () {
		$_POST = json_decode ( file_get_contents ( "php://input" ) , true ) ;
		$key_name = $_POST [ "key_name" ] ;
		$description = Product :: model () -> find ( "key_name='$key_name'" ) -> description ;
		$description = $description === null ? "..." : $description ;
		echo $description ;
	}

	public function actionAddToBasket () {
		$key_name = $_GET [ "key_name" ] ;
		$amount = $_GET [ "amount" ] ;
		$user_id = $_GET [ "user_id" ] ;

		$user_state = State :: model () -> find ( "user_id=$user_id" ) ;
		$user_basket = $user_state -> basket ;
		if ( $user_basket === null ) {
			$user_state -> basket = json_encode ( [ $key_name => $amount ] ) ;
		} else {
			$user_basket = json_decode ( $user_basket , true ) ;
			if ( isset ( $user_basket [ $key_name ] ) ) {
				$user_basket [ $key_name ] += $amount ;
			} else {
				$user_basket [ $key_name ] = $amount ;
			}

			$user_basket = json_encode ( $user_basket ) ;
			$user_state -> basket = $user_basket ;
		}
		$user_state -> save () ;
	}

	public function actionSetInBasket () {
		$_POST = json_decode ( file_get_contents ( "php://input" ) , true ) ;
		$user_id = $_POST [ "user_id" ] ;
		$key_name = $_POST [ "key_name" ] ;
		$amount = $_POST [ "amount" ] ;

		$user_state = State :: model () -> find ( "user_id=$user_id" ) ;
		$user_basket = $user_state -> basket ;
		$user_basket = json_decode ( $user_basket , true ) ;
		if ( $amount == 0 ) {
			unset ( $user_basket [ $key_name ] ) ;
			if ( $user_basket === [] ) {
				$buffer = $user_basket ;
				$user_basket = null ;
			}
		} else {
			$user_basket [ $key_name ] = $amount ;
			$buffer = $user_basket ;
		}
		$user_basket = $user_basket !== null ? json_encode ( $user_basket ) : $user_basket ;
		$user_state -> basket = $user_basket ;
		if ( $user_state -> save () === false ) {
			$response = json_encode ( [ "status" => "error" , "errorMessage" => $user_state -> getErrors () ] , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ;
			echo $response ;
		} else {
			$response = json_encode ( [ "status" => "success" , "newAmount" => isset ( $buffer [ $key_name ] ) ? $buffer [ $key_name ] : 0 ] , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) ;
			echo $response ;
		}
	}

	public function actionRemoveFromBasket () {
		$key_name = $_GET [ "key_name" ] ;
		$amount = $_GET [ "amount" ] ;
		$user_id = $_GET [ "user_id" ] ;

		$user_state = State :: model () -> find ( "user_id=$user_id" ) ;
		$user_basket = json_decode ( $user_state -> basket , true ) ;
		$user_basket [ $key_name ] -= $amount ;
		if ( $user_basket [ $key_name ] == 0 ) {
			unset ( $user_basket [ $key_name ] ) ;
			if ( $user_basket === [] ) {
				$user_basket = null ;
			}
		}

		$user_basket = $user_basket !== null ? json_encode ( $user_basket ) : $user_basket ;
		$user_state -> basket = $user_basket ;
		$user_state -> save () ;
	}

	public function actionShowBasket () {
		$_POST = json_decode ( file_get_contents ( "php://input" ) , true ) ;
		$user_id = $_POST [ "user_id" ] ;
		$color_scheme = $_POST [ "color_scheme" ] ;
		$user_basket = json_decode ( State :: model () -> find ( "user_id=$user_id" ) -> basket , true ) ;
		$basket_detailed = [] ;
		foreach ( $user_basket as $key => $value ) {
			$basket_detailed [ $key ] = [
				"object" =>  Product :: model () -> find ( "key_name='$key'" ) ,
				"amount" => $value
			]  ;
		}
		$html = generate_html ( [ "type" => "cart" , "basket_detailed" => $basket_detailed , "user_id" => $user_id , "color_scheme" => $color_scheme ] ) ;
		echo $html ;
	}

	public function actionShowOrderPage () {
		$user_id = $_GET [ "user_id" ] ;
		$html = generate_html ( [ "type" => "order" , "user_id" => $user_id ] ) ;
		echo $html ;
	}

	public function actionIsBasketEmpty () {
		$user_id = $_GET [ "user_id" ] ;
		$basket = State :: model () -> find ( "user_id=$user_id" ) -> basket ;
		if ( $basket === null || $basket === "null" ) {
			echo "true" ;
		} else {
			echo "false" ;
		}
	}

	public function actionGetPrevPage () {
		$key_name = $_GET [ "key_name" ] ;
		$level = $_GET [ "level" ] ;
		$user_id = $_GET [ "user_id" ] ;
		$color_scheme = $_GET [ "color_scheme" ] ;
		$nav_bar_text = $_GET [ "nav_bar_text" ] ;
		$arrayed_nav_bar = explode ( " > " , $nav_bar_text ) ;
		array_pop ( $arrayed_nav_bar ) ;
		$nav_bar_text = count ( $arrayed_nav_bar ) === 1 ? $arrayed_nav_bar [ 0 ] : implode ( " > " , $arrayed_nav_bar ) ;
		$lang = get_lang ( $user_id ) ;
		// echo json_encode ( $response ) ;
		if ( $level <= 0 ) {
			$html = generate_html ( [ "type" => "root" , "user_id" => $user_id , "color_scheme" => $color_scheme , "nav_bar_text" => "Main" ] ) ;
		} else {
			$level -= 1 ;
			$cat_heading = Catalog :: model () -> find ( "key_name='$key_name'" ) -> category_heading;
			$catalog = Catalog :: model () -> find ( "category_heading='$cat_heading' and level=$level" ) ;
			if ( $catalog -> type === "Catalog" || $catalog -> type === "Mix" ) {
				$prev_page = $catalog -> key_name ;
				$title = $catalog -> { "name_" . $lang } ;
				if ( $catalog -> type === "Mix" ) {
					$html = generate_html ( [ "type" => "mix" , "level" => $level , "category_heading" => $cat_heading , "title" => $title , "user_id" => $user_id , "color_scheme" => $color_scheme , "key_name" => $prev_page , "nav_bar_text" => $nav_bar_text ] ) ;
				} else {
					$html = generate_html ( [ "type" => "catalog" , "level" => $level , "category_heading" => $cat_heading , "title" => $title , "user_id" => $user_id , "color_scheme" => $color_scheme , "nav_bar_text" => $nav_bar_text ] ) ;
				}
				// $html = generate_html ( [ "type" => $type , "level" => $level , "category_heading" => $cat_heading , "title" => $title , "user_id" => $user_id , "color_scheme" => $color_scheme  ] ) ;
			} else {
				$prev_page = Catalog :: model () -> find ( "key_name='$cat_heading'" ) -> key_name ;
				$title = Catalog :: model () -> find ( "key_name='$cat_heading'" ) -> name ;
				$html = generate_html ( [ "type" => "product" , "key_name" => $key_name , "title" => $title , "user_id" => $user_id , "nav_bar_text" => $nav_bar_text ] ) ;
			}
			// $response = [ "cHeading" => $prev_page , "level" => $level ] ;
		}
		echo $html ;
	}

	public function actionGetPrevCatHeading () {
		$key_name = $_GET [ "key_name" ] ;
		$level = $_GET [ "level" ] ;

		if ( $level <= 0 ) {
			echo "root" ;
		} else {
			$level -= 1 ;
			$cat_heading = Catalog :: model () -> find ( "key_name='$key_name'" ) -> category_heading;
			$catalog = Catalog :: model () -> find ( "category_heading='$cat_heading' and level=$level" ) ;
			if ( $catalog -> type === "Catalog" ) {
				$prev_page = $catalog -> key_name ;
			} else {
				$prev_page = Catalog :: model () -> find ( "key_name='$cat_heading'" ) -> key_name ;
			}
			echo $prev_page ;
		}
	}

	public function actionSaveOrder () {
		$_POST = json_decode ( file_get_contents ( "php://input" ) , true ) ;
		$user_id = $_POST [ "user_id" ] ;
		$name = $_POST [ "name" ] ;
		$phone = $_POST [ "phone" ] ;
		$paym_type = $_POST [ "type" ] ;
		$card = $_POST [ "card" ] ;
		$basket = State :: model () -> find ( "user_id=$user_id" ) -> basket ;
		try {
			$new_record = new Orders ();
			$new_record -> user_id = $user_id;
			$new_record -> customer_name = $name;
			$new_record -> customer_phone = $phone;
			$new_record -> customer_paym_type = $paym_type;
			$new_record -> customer_card = $card;
			$new_record -> dispatched = 0 ;
			$new_record -> basket = $basket ;
			$new_record -> date = date ( "Y-m-d H:i:s" ) ;
			$saved = $new_record -> save();
		} catch (Exception $ex) {
			// file_put_contents ( $path . "/console" , "\n" . $ex->getMessage(). "\n" , FILE_APPEND ) ;
			echo json_encode ( $ex->getMessage() ) ;
		}

		rewind_basket ( $user_id ) ;
		if ( $saved === true ) {
			echo "success" ;
		} else {
			echo $new_record -> getErrors() ;
		}
	}

	public function actionSendOrderDetails () {
		$user_id = $_GET [ "user_id" ] ;
		send_order_details ( $user_id ) ;
	}

	public function actionGetProductAmount () {
		$user_id = $_GET [ "user_id" ] ;
		$key_name = $_GET [ "key_name" ] ;
		$basket = State :: model () -> find ( "user_id=$user_id" ) -> basket ;
		$arrayed_basket = json_decode ( $basket , true ) ;
		echo isset ( $arrayed_basket [ $key_name ] ) ? $arrayed_basket [ $key_name ] : 0 ;
	}
}
