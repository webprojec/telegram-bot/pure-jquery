<?php
	class TelegramController extends Controller {

			public function actionIndex () {
				$this -> layout = "main" ;
				$this -> render ( "index" ) ;
			}

			public function actionHandleUpdates () {
				handle_input ( file_get_contents ( "php://input" ) ) ;
			}

	    public function actionSendMessage () {
	        $user_id = $_GET [ "user_id" ] ;
	        $text = $_GET [ "text" ] ;
	        send_message ( $user_id , $text ) ;
	    }

	    public function actionWebApp () {
	        $user_id = $_GET [ "user_id" ] ;
	        // $user_id = 510469754 ;
					$labels = file_get_contents ( ROOT_PATH . "/labels.json" ) ;
					$lang = get_lang ( $user_id ) ;
	        $this -> layout = "webapp" ;
	        $this -> render ( "webapp" , [ "vars" => [ "user_id" => $user_id , "labels" => $labels , "lang" => $lang ] ] ) ;
	        // $this -> render ( "console" , [ "console" => json_encode ($_GET , JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) ] ) ;
	    }
	}
?>
